import { createElement, removeClass, addClass } from './helpers/domHelpers';
import { handleRoomsUpdate, handleRoomUpdate, handleUserStateUpdate } from './handlers/room-update';
import { handleJoinRoom, handleTimerStart, handleChallengeStart, handleChalangeEnd, leaveRoom } from './handlers/room-interact';
import { GameResult, User, Room } from './types/index';
import { handleUserNameNotUnique } from './handlers/error-handlers';
import { formateInputTextInner, calcProgress } from './helpers/domFormatters';
import { actions } from './constants/actions';

if (window.location.pathname === '/login') {
  const username: string | null = sessionStorage.getItem('username');

    if (username) {
        window.location.replace('/game');
    }

    const submitButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById('submit-button');
    const input: HTMLInputElement = <HTMLInputElement>document.getElementById('username-input');

    const getInputValue = () => input?.value;

    const onClickSubmitButton = () => {
        const inputValue: string | null = getInputValue();
        if (!inputValue) {
            return;
        }
        sessionStorage.setItem('username', inputValue);
        window.location.replace('/game');
    };

    const onKeyUp = (ev: KeyboardEvent) => {
        const enterKeyCode: number = 13;
        if (ev.keyCode === enterKeyCode) {
            submitButton?.click();
        }
    };

    submitButton?.addEventListener('click', onClickSubmitButton);
    window.addEventListener('keyup', onKeyUp);
} else {

  const username: string | null = sessionStorage.getItem('username');
  let activeRoom: Room | null = null;
  let textToType: string = '';
  let handlerFunc: ((event: KeyboardEvent) => void) | null = null;

  if (!username) {
      window.location.replace('/login');
  };

  const socket: SocketIOClient.Socket = io('', { query: { username } });
  
  socket.on(actions.roomNameNotUnique, (message: string) => window.alert(message));
  socket.on(actions.userNameNotUnique, (message: string) => handleUserNameNotUnique(message))
  socket.on(actions.updateRooms, (rooms: Room[]) => handleRoomsUpdate(rooms, socket));
  socket.on(actions.userJoined, (room: Room) => activeRoom = handleJoinRoom(room, username!));
  socket.on(actions.activeRoomUpdate, (room: Room) => activeRoom = handleRoomUpdate(room, username!));
  socket.on(actions.userStateUpdate, (user: User) => activeRoom = handleUserStateUpdate(user, activeRoom!));
  socket.on(actions.usersReady, (data: { seconds: number, textIdx: number }) => {
    handleTimerStart(data).then((text: string) => {
      textToType = text;
    });
  })
  socket.on(actions.startChallenge, (allowedSeconds: number) => {
    const result = handleChallengeStart(allowedSeconds, textToType);
    if (result) {
      const { textToType, textElement, seconds } = result;
      handlerFunc = makeTextInputHandler(textToType, textElement, seconds);
      document.addEventListener('keydown', handlerFunc);
    }
  });
  socket.on(actions.endChallenge, (gameResult: GameResult[]) => {
    handleChalangeEnd(gameResult, handlerFunc as any, textToType);
    textToType = '';
  });

  const onClickCreateRoom = () => {
      const roomName = prompt('Please, input room name ');
      if (roomName) {
          socket.emit(actions.createRoom, roomName);
      }
  };

  const onClickReady = (event: MouseEvent) => {
    const button: HTMLElement = <HTMLElement>event.target;
    const user: User = activeRoom!.users!.filter(user => user.userName === username)[0];
    if (user) {
      const data = { isReady: !user.progress.isReady, inputtedText: '' };
      socket.emit(actions.toggleReady, data);
      button.textContent = data.isReady ? 'Not Ready' : 'Ready';
    }
  }

  const makeTextInputHandler = (text: string, viewerElement: HTMLElement, seconds: { secondsPassed: number }) => {
    const user = activeRoom!.users!.filter(user => user.userName === username)[0];

    return (event: KeyboardEvent) => {
      // if user typed whole text we should no longer let him input data
      if (!user.progress.spentSeconds && (event.key.length === 1 || event.key === 'Backspace')) {

        if (event.key === 'Backspace') {
          user.progress.inputtedText = user.progress.inputtedText.substring(0, user.progress.inputtedText.length - 1);
        } else if(user.progress.inputtedText.length < text.length) {
          user.progress.inputtedText += event.key;
        }
        viewerElement.innerHTML = formateInputTextInner(text, user.progress.inputtedText);
        user.progress.doneInPerc = calcProgress(text, user.progress.inputtedText);
        if (user.progress.doneInPerc === 100) {
          user.progress.spentSeconds = seconds.secondsPassed;
        }
        socket.emit(actions.userStateUpdate, user.progress);
      }
    }
  };

  const createRoomButton: HTMLElement = <HTMLElement>document.getElementById('create-room-button');
  createRoomButton.addEventListener('click', onClickCreateRoom);

  const leaveRoomButton: HTMLElement = <HTMLElement>document.querySelector('.game-page__back-to-rooms-button');
  leaveRoomButton.addEventListener('click', () => leaveRoom(socket));

  const readyButton: HTMLElement = <HTMLElement>document.querySelector('.ready-column__ready-button');
  readyButton.addEventListener('click', onClickReady);
}