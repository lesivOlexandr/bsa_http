import { User, GameResult } from '../types/index';

export const formateRoomUserView = ({ userName, progress }: User, currentUserName: string) => {
  return (`
    <div class="users-container__user ${userName === currentUserName ? 'user__exact': ''}">
      <div class="user__meta">
        <div class="${progress.isReady ? 'ready': 'nready'} circle">

        </div>
      <span class="user-meta__name">
        ${userName} ${userName === currentUserName ? '<span>(You)</span>': ''}
      </span>
      </div>
      <div class="user__progress">
        <div 
          class="progress__indicator ${progress.doneInPerc === 100 ? 'progress__indicator-done' : ''}"
          style="width: ${ progress.doneInPerc || 0 }%">
        </div>
      </div>
    </div>
  `);
};

export const formateInputTextInner = (text: string, inputted: string) => {
  let index = 0;
  let innerHTML = '';
  let lastWasCorrect = null;

  for (let char of inputted) {
    if (index < text.length && char === text[index]) {
      if (lastWasCorrect === null) {
        innerHTML += '<span class="correct">';
      } else if (lastWasCorrect === false) {
        innerHTML += '</span><span class="correct">'
      }
      lastWasCorrect = true;
    } else {
      if (lastWasCorrect === null) {
        innerHTML += '<span class="incorrect">';
      } else if (lastWasCorrect === true) {
        innerHTML += '</span><span class="incorrect">';
      }
      lastWasCorrect = false;
    }
    innerHTML += text[index];
    index++;
  }
  if (innerHTML) {
    innerHTML += '</span>';
  }
  if (inputted.length !== text.length) {
    innerHTML += `<span class="next">${text[inputted.length]}</span>`
  }
  innerHTML += text.substring(inputted.length + 1);

  return innerHTML;
}

export const showModal = (gameResult: GameResult[], textToType: string) => {
  const userResultEls = gameResult.map(result => (`
    <li> ${result.username} | ${result.spentSeconds && (textToType.length / result.spentSeconds).toFixed(2) || '-'} symbols per second</li>
  `))
  const modal = `
    <div class="modal-backdrop">
    <div class="modal">
      <div class="modal__header">
        <p>Game Results</p>
        <span class="modal__close-icon"></span>
      </div>
      <ol class="modal__user-list">
      ${userResultEls.join('')}
      </ol>
      </div>
    </div>
  `;

  document.body.insertAdjacentHTML('beforeend', modal);
}

export const calcProgress = (text: string, input: string) => {
  if (!text?.length || !input?.length) {
    return 0;
  }

  let count = 0;
  for (let char of input) {
    if (char === text[count]) {
      count += 1;
    }
  }
  return count * 100 / text.length;
}
