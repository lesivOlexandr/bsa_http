import { createElement, removeClass, addClass } from '../helpers/domHelpers';
import { handleRoomUpdate } from './room-update';
import { formateInputTextInner, showModal } from '../helpers/domFormatters';
import { Room, GameResult } from '../types/index';
import { actions } from '../constants/actions';

export const handleJoinRoom = (room: Room, currentUserName: string): Room => {
  const roomsPage: HTMLElement = <HTMLElement>document.querySelector('.rooms-page');
  const gamePage: HTMLElement = <HTMLElement>document.querySelector('.game-page');
  addClass(roomsPage, 'display-none');
  removeClass(gamePage, 'display-none');

  return handleRoomUpdate(room, currentUserName);
};

export const leaveRoom = (socket: SocketIOClient.Socket): void => {
  const roomsPage: HTMLElement = <HTMLElement>document.querySelector('.rooms-page');
  const gamePage: HTMLElement = <HTMLElement>document.querySelector('.game-page');
  removeClass(roomsPage, 'display-none');
  addClass(gamePage, 'display-none');
  socket.emit(actions.leaveRoom);
}

export const handleTimerStart = ({ seconds, textIdx }: { seconds: number, textIdx: number }): Promise<string> => {
  const timerParent: HTMLElement = <HTMLElement>document.querySelector('.game-page__ready-column');
  const readyButton: HTMLElement = <HTMLElement>document.querySelector('.ready-column__ready-button');
  const backtoRoomsButton: HTMLElement = <HTMLElement>document.querySelector('.game-page__back-to-rooms-button');

  readyButton.hidden = true;
  backtoRoomsButton.hidden = true;

  const timerElement: HTMLElement = createElement({ tagName: 'span' });
  timerElement.textContent = String(seconds);
  timerParent.append(timerElement)

  const intervalId = setInterval(() => {
    if (seconds > 1) {
      timerElement.textContent = String(--seconds);
    } else {
      timerParent.removeChild(timerElement);
      clearInterval(intervalId);
    }
  }, 1000);

  return fetch(`/game/texts/${textIdx}`).then(res => res.text());
}

export const handleChallengeStart = (allowedSeconds: number, textToType: string) => {
  const seconds = { allowedSeconds, secondsPassed: 0 };
  const column: HTMLElement = <HTMLElement>document.querySelector('.game-page__ready-column');
  const timerElement: HTMLElement = createElement({ tagName: 'span', className: 'ready-column__game-timer' });
  const textElement: HTMLElement = createElement({ tagName: 'span', className: 'ready-column__text' });
  timerElement.textContent = allowedSeconds + ' seconds left';

  const intervalId = setInterval(() => {
    if (seconds.allowedSeconds > seconds.secondsPassed) {
      timerElement.textContent = (seconds.allowedSeconds - ++seconds.secondsPassed) + ' seconds left';
    } else {
      clearInterval(intervalId);
    }
  }, 1000);
  if (textToType) {
    textElement.innerHTML = formateInputTextInner(textToType, '');
    column.append(timerElement, textElement);
    return { textToType, textElement, seconds };
  }
  return null;
}

export const handleChalangeEnd = (gameResult: GameResult[], handlerFunc: (event: KeyboardEvent) => void, textToType: string) => {
  document.removeEventListener('keydown', handlerFunc);
  showModal(gameResult, textToType);
  const closeModalEl: HTMLElement = <HTMLElement>document.querySelector('.modal__close-icon');
  const modalEl: HTMLElement = <HTMLElement>document.querySelector('.modal-backdrop');
  closeModalEl.addEventListener('click', () => modalEl.remove());

  document.querySelectorAll('.ready-column__game-timer, .ready-column__text').forEach(el => el.remove());
  const readyButton: HTMLElement = <HTMLElement>document.querySelector('#ready-button');
  const backtoRoomsButton: HTMLElement = <HTMLElement>document.querySelector('.game-page__back-to-rooms-button');
  readyButton.hidden = false;
  readyButton.textContent = 'Ready';
  backtoRoomsButton.hidden = false;
}
