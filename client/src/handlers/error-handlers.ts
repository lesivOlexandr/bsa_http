export const handleUserNameNotUnique = (message: string) => {
  alert(message);
  sessionStorage.removeItem('username');
  window.location.replace('/login');
}