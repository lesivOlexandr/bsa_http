import { createElement, } from '../helpers/domHelpers';
import { formateRoomUserView } from '../helpers/domFormatters';
import { User, Room } from '../types/index';
import { actions } from '../constants/actions';

export const handleRoomsUpdate = (rooms: Room[], socket: SocketIOClient.Socket) => {
  const roomsContainerPrev = document.querySelector('.rooms-page__rooms-container');
  const roomContainerNext = createElement({ tagName: 'div', className: 'rooms-page__rooms-container rooms-page__item' });
  const roomElements = rooms.map(room => {
    const roomContainer = createElement({ tagName: 'div', className: 'room-container__room' });
    const roomUsersCount = createElement({ tagName: 'p', className: 'room__users-count' });
    const roomName = createElement({ tagName: 'h2', className:'room__name' });
    const roomJoin = createElement({ tagName: 'button', className: 'room__join' });
    roomUsersCount.textContent = `${room.usersCount} user connected`;
    roomName.textContent = room.roomName;
    roomJoin.textContent = 'Join';
    roomJoin.addEventListener('click', () => socket.emit(actions.joinRoom, room.roomName));
    roomContainer.append(roomUsersCount, roomName, roomJoin);
    return roomContainer;
  });
  roomContainerNext.append(...roomElements);
  roomsContainerPrev?.replaceWith(roomContainerNext);
  return rooms;
};

export const handleRoomUpdate = (room: Room, currentUserName: string): Room => {
  const usersContainerOld: HTMLElement = <HTMLElement>document.querySelector('.game-page__users-container');
  const userElements: string[] = room.users!.map((user: User) => formateRoomUserView(user, currentUserName));
  usersContainerOld!.innerHTML = userElements.join('');
  return room;
}

export const handleUserStateUpdate = (updatedUser: User, activeRoom: Room): Room | null => {
  const user: User = activeRoom.users!.filter((user: User) => user.userName === updatedUser.userName)[0];
  if (user) {
    (<any>Object).assign(user, updatedUser);
    return handleRoomUpdate(activeRoom, user.userName);
  }
  return null;
}