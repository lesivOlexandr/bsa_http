const path = require('path');

module.exports = {
  entry: './src/index.ts',
  output: {
    path: path.resolve(__dirname, '../server/build/javascript'), 
    filename: 'bundle.js',
    publicPath: '../server/build/javascript'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.(png|jpg)$/,
        loader: 'url-loader'
      },
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: [/node_modules/],
      },
    ]
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  mode: 'development',
  devtool: "source-map"
}