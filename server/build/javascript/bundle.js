/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "../server/build/javascript";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/constants/actions.ts":
/*!**********************************!*\
  !*** ./src/constants/actions.ts ***!
  \**********************************/
/*! exports provided: actions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "actions", function() { return actions; });
var actions;
(function (actions) {
    actions["roomNameNotUnique"] = "ERROR_ROOM_NAME_IS_NOT_UNIQUE";
    actions["userNameNotUnique"] = "ERROR_USER_NAME_IS_NOT_UNIQUE";
    actions["updateRooms"] = "UPDATE_ROOMS";
    actions["userJoined"] = "USER_JOINED";
    actions["activeRoomUpdate"] = "ACTIVE_ROOM_UPDATE";
    actions["userStateUpdate"] = "USER_STATE_UPDATE";
    actions["usersReady"] = "ALL_ROOM_USERS_READY";
    actions["startChallenge"] = "START_CHALLENGE";
    actions["endChallenge"] = "END_CHALLENGE";
    actions["createRoom"] = "CREATE_ROOM";
    actions["toggleReady"] = "TOGGLE_READY";
    actions["joinRoom"] = "JOIN_ROOM";
    actions["leaveRoom"] = "LEAVE_ROOM";
})(actions || (actions = {}));


/***/ }),

/***/ "./src/handlers/error-handlers.ts":
/*!****************************************!*\
  !*** ./src/handlers/error-handlers.ts ***!
  \****************************************/
/*! exports provided: handleUserNameNotUnique */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleUserNameNotUnique", function() { return handleUserNameNotUnique; });
var handleUserNameNotUnique = function (message) {
    alert(message);
    sessionStorage.removeItem('username');
    window.location.replace('/login');
};


/***/ }),

/***/ "./src/handlers/room-interact.ts":
/*!***************************************!*\
  !*** ./src/handlers/room-interact.ts ***!
  \***************************************/
/*! exports provided: handleJoinRoom, leaveRoom, handleTimerStart, handleChallengeStart, handleChalangeEnd */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleJoinRoom", function() { return handleJoinRoom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "leaveRoom", function() { return leaveRoom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleTimerStart", function() { return handleTimerStart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleChallengeStart", function() { return handleChallengeStart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleChalangeEnd", function() { return handleChalangeEnd; });
/* harmony import */ var _helpers_domHelpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../helpers/domHelpers */ "./src/helpers/domHelpers.ts");
/* harmony import */ var _room_update__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./room-update */ "./src/handlers/room-update.ts");
/* harmony import */ var _helpers_domFormatters__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../helpers/domFormatters */ "./src/helpers/domFormatters.ts");
/* harmony import */ var _constants_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants/actions */ "./src/constants/actions.ts");




var handleJoinRoom = function (room, currentUserName) {
    var roomsPage = document.querySelector('.rooms-page');
    var gamePage = document.querySelector('.game-page');
    Object(_helpers_domHelpers__WEBPACK_IMPORTED_MODULE_0__["addClass"])(roomsPage, 'display-none');
    Object(_helpers_domHelpers__WEBPACK_IMPORTED_MODULE_0__["removeClass"])(gamePage, 'display-none');
    return Object(_room_update__WEBPACK_IMPORTED_MODULE_1__["handleRoomUpdate"])(room, currentUserName);
};
var leaveRoom = function (socket) {
    var roomsPage = document.querySelector('.rooms-page');
    var gamePage = document.querySelector('.game-page');
    Object(_helpers_domHelpers__WEBPACK_IMPORTED_MODULE_0__["removeClass"])(roomsPage, 'display-none');
    Object(_helpers_domHelpers__WEBPACK_IMPORTED_MODULE_0__["addClass"])(gamePage, 'display-none');
    socket.emit(_constants_actions__WEBPACK_IMPORTED_MODULE_3__["actions"].leaveRoom);
};
var handleTimerStart = function (_a) {
    var seconds = _a.seconds, textIdx = _a.textIdx;
    var timerParent = document.querySelector('.game-page__ready-column');
    var readyButton = document.querySelector('.ready-column__ready-button');
    var backtoRoomsButton = document.querySelector('.game-page__back-to-rooms-button');
    readyButton.hidden = true;
    backtoRoomsButton.hidden = true;
    var timerElement = Object(_helpers_domHelpers__WEBPACK_IMPORTED_MODULE_0__["createElement"])({ tagName: 'span' });
    timerElement.textContent = String(seconds);
    timerParent.append(timerElement);
    var intervalId = setInterval(function () {
        if (seconds > 1) {
            timerElement.textContent = String(--seconds);
        }
        else {
            timerParent.removeChild(timerElement);
            clearInterval(intervalId);
        }
    }, 1000);
    return fetch("/game/texts/" + textIdx).then(function (res) { return res.text(); });
};
var handleChallengeStart = function (allowedSeconds, textToType) {
    var seconds = { allowedSeconds: allowedSeconds, secondsPassed: 0 };
    var column = document.querySelector('.game-page__ready-column');
    var timerElement = Object(_helpers_domHelpers__WEBPACK_IMPORTED_MODULE_0__["createElement"])({ tagName: 'span', className: 'ready-column__game-timer' });
    var textElement = Object(_helpers_domHelpers__WEBPACK_IMPORTED_MODULE_0__["createElement"])({ tagName: 'span', className: 'ready-column__text' });
    timerElement.textContent = allowedSeconds + ' seconds left';
    var intervalId = setInterval(function () {
        if (seconds.allowedSeconds > seconds.secondsPassed) {
            timerElement.textContent = (seconds.allowedSeconds - ++seconds.secondsPassed) + ' seconds left';
        }
        else {
            clearInterval(intervalId);
        }
    }, 1000);
    if (textToType) {
        textElement.innerHTML = Object(_helpers_domFormatters__WEBPACK_IMPORTED_MODULE_2__["formateInputTextInner"])(textToType, '');
        column.append(timerElement, textElement);
        return { textToType: textToType, textElement: textElement, seconds: seconds };
    }
    return null;
};
var handleChalangeEnd = function (gameResult, handlerFunc, textToType) {
    document.removeEventListener('keydown', handlerFunc);
    Object(_helpers_domFormatters__WEBPACK_IMPORTED_MODULE_2__["showModal"])(gameResult, textToType);
    var closeModalEl = document.querySelector('.modal__close-icon');
    var modalEl = document.querySelector('.modal-backdrop');
    closeModalEl.addEventListener('click', function () { return modalEl.remove(); });
    document.querySelectorAll('.ready-column__game-timer, .ready-column__text').forEach(function (el) { return el.remove(); });
    var readyButton = document.querySelector('#ready-button');
    var backtoRoomsButton = document.querySelector('.game-page__back-to-rooms-button');
    readyButton.hidden = false;
    readyButton.textContent = 'Ready';
    backtoRoomsButton.hidden = false;
};


/***/ }),

/***/ "./src/handlers/room-update.ts":
/*!*************************************!*\
  !*** ./src/handlers/room-update.ts ***!
  \*************************************/
/*! exports provided: handleRoomsUpdate, handleRoomUpdate, handleUserStateUpdate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleRoomsUpdate", function() { return handleRoomsUpdate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleRoomUpdate", function() { return handleRoomUpdate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleUserStateUpdate", function() { return handleUserStateUpdate; });
/* harmony import */ var _helpers_domHelpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../helpers/domHelpers */ "./src/helpers/domHelpers.ts");
/* harmony import */ var _helpers_domFormatters__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../helpers/domFormatters */ "./src/helpers/domFormatters.ts");
/* harmony import */ var _constants_actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../constants/actions */ "./src/constants/actions.ts");
var __read = (undefined && undefined.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (undefined && undefined.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};



var handleRoomsUpdate = function (rooms, socket) {
    var roomsContainerPrev = document.querySelector('.rooms-page__rooms-container');
    var roomContainerNext = Object(_helpers_domHelpers__WEBPACK_IMPORTED_MODULE_0__["createElement"])({ tagName: 'div', className: 'rooms-page__rooms-container rooms-page__item' });
    var roomElements = rooms.map(function (room) {
        var roomContainer = Object(_helpers_domHelpers__WEBPACK_IMPORTED_MODULE_0__["createElement"])({ tagName: 'div', className: 'room-container__room' });
        var roomUsersCount = Object(_helpers_domHelpers__WEBPACK_IMPORTED_MODULE_0__["createElement"])({ tagName: 'p', className: 'room__users-count' });
        var roomName = Object(_helpers_domHelpers__WEBPACK_IMPORTED_MODULE_0__["createElement"])({ tagName: 'h2', className: 'room__name' });
        var roomJoin = Object(_helpers_domHelpers__WEBPACK_IMPORTED_MODULE_0__["createElement"])({ tagName: 'button', className: 'room__join' });
        roomUsersCount.textContent = room.usersCount + " user connected";
        roomName.textContent = room.roomName;
        roomJoin.textContent = 'Join';
        roomJoin.addEventListener('click', function () { return socket.emit(_constants_actions__WEBPACK_IMPORTED_MODULE_2__["actions"].joinRoom, room.roomName); });
        roomContainer.append(roomUsersCount, roomName, roomJoin);
        return roomContainer;
    });
    roomContainerNext.append.apply(roomContainerNext, __spread(roomElements));
    roomsContainerPrev === null || roomsContainerPrev === void 0 ? void 0 : roomsContainerPrev.replaceWith(roomContainerNext);
    return rooms;
};
var handleRoomUpdate = function (room, currentUserName) {
    var usersContainerOld = document.querySelector('.game-page__users-container');
    var userElements = room.users.map(function (user) { return Object(_helpers_domFormatters__WEBPACK_IMPORTED_MODULE_1__["formateRoomUserView"])(user, currentUserName); });
    usersContainerOld.innerHTML = userElements.join('');
    return room;
};
var handleUserStateUpdate = function (updatedUser, activeRoom) {
    var user = activeRoom.users.filter(function (user) { return user.userName === updatedUser.userName; })[0];
    if (user) {
        Object.assign(user, updatedUser);
        return handleRoomUpdate(activeRoom, user.userName);
    }
    return null;
};


/***/ }),

/***/ "./src/helpers/domFormatters.ts":
/*!**************************************!*\
  !*** ./src/helpers/domFormatters.ts ***!
  \**************************************/
/*! exports provided: formateRoomUserView, formateInputTextInner, showModal, calcProgress */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formateRoomUserView", function() { return formateRoomUserView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formateInputTextInner", function() { return formateInputTextInner; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showModal", function() { return showModal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calcProgress", function() { return calcProgress; });
var __values = (undefined && undefined.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
var formateRoomUserView = function (_a, currentUserName) {
    var userName = _a.userName, progress = _a.progress;
    return ("\n    <div class=\"users-container__user " + (userName === currentUserName ? 'user__exact' : '') + "\">\n      <div class=\"user__meta\">\n        <div class=\"" + (progress.isReady ? 'ready' : 'nready') + " circle\">\n\n        </div>\n      <span class=\"user-meta__name\">\n        " + userName + " " + (userName === currentUserName ? '<span>(You)</span>' : '') + "\n      </span>\n      </div>\n      <div class=\"user__progress\">\n        <div \n          class=\"progress__indicator " + (progress.doneInPerc === 100 ? 'progress__indicator-done' : '') + "\"\n          style=\"width: " + (progress.doneInPerc || 0) + "%\">\n        </div>\n      </div>\n    </div>\n  ");
};
var formateInputTextInner = function (text, inputted) {
    var e_1, _a;
    var index = 0;
    var innerHTML = '';
    var lastWasCorrect = null;
    try {
        for (var inputted_1 = __values(inputted), inputted_1_1 = inputted_1.next(); !inputted_1_1.done; inputted_1_1 = inputted_1.next()) {
            var char = inputted_1_1.value;
            if (index < text.length && char === text[index]) {
                if (lastWasCorrect === null) {
                    innerHTML += '<span class="correct">';
                }
                else if (lastWasCorrect === false) {
                    innerHTML += '</span><span class="correct">';
                }
                lastWasCorrect = true;
            }
            else {
                if (lastWasCorrect === null) {
                    innerHTML += '<span class="incorrect">';
                }
                else if (lastWasCorrect === true) {
                    innerHTML += '</span><span class="incorrect">';
                }
                lastWasCorrect = false;
            }
            innerHTML += text[index];
            index++;
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (inputted_1_1 && !inputted_1_1.done && (_a = inputted_1.return)) _a.call(inputted_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
    if (innerHTML) {
        innerHTML += '</span>';
    }
    if (inputted.length !== text.length) {
        innerHTML += "<span class=\"next\">" + text[inputted.length] + "</span>";
    }
    innerHTML += text.substring(inputted.length + 1);
    return innerHTML;
};
var showModal = function (gameResult, textToType) {
    var userResultEls = gameResult.map(function (result) { return ("\n    <li> " + result.username + " | " + (result.spentSeconds && (textToType.length / result.spentSeconds).toFixed(2) || '-') + " symbols per second</li>\n  "); });
    var modal = "\n    <div class=\"modal-backdrop\">\n    <div class=\"modal\">\n      <div class=\"modal__header\">\n        <p>Game Results</p>\n        <span class=\"modal__close-icon\"></span>\n      </div>\n      <ol class=\"modal__user-list\">\n      " + userResultEls.join('') + "\n      </ol>\n      </div>\n    </div>\n  ";
    document.body.insertAdjacentHTML('beforeend', modal);
};
var calcProgress = function (text, input) {
    var e_2, _a;
    if (!(text === null || text === void 0 ? void 0 : text.length) || !(input === null || input === void 0 ? void 0 : input.length)) {
        return 0;
    }
    var count = 0;
    try {
        for (var input_1 = __values(input), input_1_1 = input_1.next(); !input_1_1.done; input_1_1 = input_1.next()) {
            var char = input_1_1.value;
            if (char === text[count]) {
                count += 1;
            }
        }
    }
    catch (e_2_1) { e_2 = { error: e_2_1 }; }
    finally {
        try {
            if (input_1_1 && !input_1_1.done && (_a = input_1.return)) _a.call(input_1);
        }
        finally { if (e_2) throw e_2.error; }
    }
    return count * 100 / text.length;
};


/***/ }),

/***/ "./src/helpers/domHelpers.ts":
/*!***********************************!*\
  !*** ./src/helpers/domHelpers.ts ***!
  \***********************************/
/*! exports provided: createElement, addClass, removeClass, formatClassNames */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createElement", function() { return createElement; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addClass", function() { return addClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "removeClass", function() { return removeClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formatClassNames", function() { return formatClassNames; });
var __read = (undefined && undefined.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (undefined && undefined.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var createElement = function (_a) {
    var tagName = _a.tagName, className = _a.className, _b = _a.attributes, attributes = _b === void 0 ? {} : _b;
    var element = document.createElement(tagName);
    if (className) {
        addClass(element, className);
    }
    Object.keys(attributes).forEach(function (key) { return element.setAttribute(key, attributes[key]); });
    return element;
};
var addClass = function (element, className) {
    var _a;
    var classNames = formatClassNames(className);
    (_a = element.classList).add.apply(_a, __spread(classNames));
};
var removeClass = function (element, className) {
    var _a;
    var classNames = formatClassNames(className);
    (_a = element.classList).remove.apply(_a, __spread(classNames));
};
var formatClassNames = function (className) { return className.split(" ").filter(Boolean); };


/***/ }),

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _handlers_room_update__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./handlers/room-update */ "./src/handlers/room-update.ts");
/* harmony import */ var _handlers_room_interact__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./handlers/room-interact */ "./src/handlers/room-interact.ts");
/* harmony import */ var _handlers_error_handlers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./handlers/error-handlers */ "./src/handlers/error-handlers.ts");
/* harmony import */ var _helpers_domFormatters__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./helpers/domFormatters */ "./src/helpers/domFormatters.ts");
/* harmony import */ var _constants_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./constants/actions */ "./src/constants/actions.ts");





if (window.location.pathname === '/login') {
    var username = sessionStorage.getItem('username');
    if (username) {
        window.location.replace('/game');
    }
    var submitButton_1 = document.getElementById('submit-button');
    var input_1 = document.getElementById('username-input');
    var getInputValue_1 = function () { return input_1 === null || input_1 === void 0 ? void 0 : input_1.value; };
    var onClickSubmitButton = function () {
        var inputValue = getInputValue_1();
        if (!inputValue) {
            return;
        }
        sessionStorage.setItem('username', inputValue);
        window.location.replace('/game');
    };
    var onKeyUp = function (ev) {
        var enterKeyCode = 13;
        if (ev.keyCode === enterKeyCode) {
            submitButton_1 === null || submitButton_1 === void 0 ? void 0 : submitButton_1.click();
        }
    };
    submitButton_1 === null || submitButton_1 === void 0 ? void 0 : submitButton_1.addEventListener('click', onClickSubmitButton);
    window.addEventListener('keyup', onKeyUp);
}
else {
    var username_1 = sessionStorage.getItem('username');
    var activeRoom_1 = null;
    var textToType_1 = '';
    var handlerFunc_1 = null;
    if (!username_1) {
        window.location.replace('/login');
    }
    ;
    var socket_1 = io('', { query: { username: username_1 } });
    socket_1.on(_constants_actions__WEBPACK_IMPORTED_MODULE_4__["actions"].roomNameNotUnique, function (message) { return window.alert(message); });
    socket_1.on(_constants_actions__WEBPACK_IMPORTED_MODULE_4__["actions"].userNameNotUnique, function (message) { return Object(_handlers_error_handlers__WEBPACK_IMPORTED_MODULE_2__["handleUserNameNotUnique"])(message); });
    socket_1.on(_constants_actions__WEBPACK_IMPORTED_MODULE_4__["actions"].updateRooms, function (rooms) { return Object(_handlers_room_update__WEBPACK_IMPORTED_MODULE_0__["handleRoomsUpdate"])(rooms, socket_1); });
    socket_1.on(_constants_actions__WEBPACK_IMPORTED_MODULE_4__["actions"].userJoined, function (room) { return activeRoom_1 = Object(_handlers_room_interact__WEBPACK_IMPORTED_MODULE_1__["handleJoinRoom"])(room, username_1); });
    socket_1.on(_constants_actions__WEBPACK_IMPORTED_MODULE_4__["actions"].activeRoomUpdate, function (room) { return activeRoom_1 = Object(_handlers_room_update__WEBPACK_IMPORTED_MODULE_0__["handleRoomUpdate"])(room, username_1); });
    socket_1.on(_constants_actions__WEBPACK_IMPORTED_MODULE_4__["actions"].userStateUpdate, function (user) { return activeRoom_1 = Object(_handlers_room_update__WEBPACK_IMPORTED_MODULE_0__["handleUserStateUpdate"])(user, activeRoom_1); });
    socket_1.on(_constants_actions__WEBPACK_IMPORTED_MODULE_4__["actions"].usersReady, function (data) {
        Object(_handlers_room_interact__WEBPACK_IMPORTED_MODULE_1__["handleTimerStart"])(data).then(function (text) {
            textToType_1 = text;
        });
    });
    socket_1.on(_constants_actions__WEBPACK_IMPORTED_MODULE_4__["actions"].startChallenge, function (allowedSeconds) {
        var result = Object(_handlers_room_interact__WEBPACK_IMPORTED_MODULE_1__["handleChallengeStart"])(allowedSeconds, textToType_1);
        if (result) {
            var textToType_2 = result.textToType, textElement = result.textElement, seconds = result.seconds;
            handlerFunc_1 = makeTextInputHandler_1(textToType_2, textElement, seconds);
            document.addEventListener('keydown', handlerFunc_1);
        }
    });
    socket_1.on(_constants_actions__WEBPACK_IMPORTED_MODULE_4__["actions"].endChallenge, function (gameResult) {
        Object(_handlers_room_interact__WEBPACK_IMPORTED_MODULE_1__["handleChalangeEnd"])(gameResult, handlerFunc_1, textToType_1);
        textToType_1 = '';
    });
    var onClickCreateRoom = function () {
        var roomName = prompt('Please, input room name ');
        if (roomName) {
            socket_1.emit(_constants_actions__WEBPACK_IMPORTED_MODULE_4__["actions"].createRoom, roomName);
        }
    };
    var onClickReady = function (event) {
        var button = event.target;
        var user = activeRoom_1.users.filter(function (user) { return user.userName === username_1; })[0];
        if (user) {
            var data = { isReady: !user.progress.isReady, inputtedText: '' };
            socket_1.emit(_constants_actions__WEBPACK_IMPORTED_MODULE_4__["actions"].toggleReady, data);
            button.textContent = data.isReady ? 'Not Ready' : 'Ready';
        }
    };
    var makeTextInputHandler_1 = function (text, viewerElement, seconds) {
        var user = activeRoom_1.users.filter(function (user) { return user.userName === username_1; })[0];
        return function (event) {
            if (!user.progress.spentSeconds && (event.key.length === 1 || event.key === 'Backspace')) {
                if (event.key === 'Backspace') {
                    user.progress.inputtedText = user.progress.inputtedText.substring(0, user.progress.inputtedText.length - 1);
                }
                else if (user.progress.inputtedText.length < text.length) {
                    user.progress.inputtedText += event.key;
                }
                viewerElement.innerHTML = Object(_helpers_domFormatters__WEBPACK_IMPORTED_MODULE_3__["formateInputTextInner"])(text, user.progress.inputtedText);
                user.progress.doneInPerc = Object(_helpers_domFormatters__WEBPACK_IMPORTED_MODULE_3__["calcProgress"])(text, user.progress.inputtedText);
                if (user.progress.doneInPerc === 100) {
                    user.progress.spentSeconds = seconds.secondsPassed;
                }
                socket_1.emit(_constants_actions__WEBPACK_IMPORTED_MODULE_4__["actions"].userStateUpdate, user.progress);
            }
        };
    };
    var createRoomButton = document.getElementById('create-room-button');
    createRoomButton.addEventListener('click', onClickCreateRoom);
    var leaveRoomButton = document.querySelector('.game-page__back-to-rooms-button');
    leaveRoomButton.addEventListener('click', function () { return Object(_handlers_room_interact__WEBPACK_IMPORTED_MODULE_1__["leaveRoom"])(socket_1); });
    var readyButton = document.querySelector('.ready-column__ready-button');
    readyButton.addEventListener('click', onClickReady);
}


/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map