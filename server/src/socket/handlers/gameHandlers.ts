import { connectedUsers, rooms, roomIntervalMapping, userRoomMapping } from '../../shared';
import { Socket, Server as ioServer } from 'socket.io';
import { Room, Progress, GameResult, User } from '../../types';
import { MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME } from '../config';
import { actions } from '../../constants/actions';
import { getRandomNumber, calcProgress } from '../../helpers/socketHelpers';
import { texts } from '../../data';

export const updateRooms = (io: ioServer): void => {
  const joinableRooms: Room[] = rooms.filter((room: Room) => !room.maxUsersReached && room.state === 'pending');
  // at this point we send only quantity of users instead of users themself to avoid redudancy
  const roomToSend: Room[] = joinableRooms.map(room => ({ roomName: room.roomName, usersCount: room?.users?.length || 0 }));
  io.emit(actions.updateRooms, roomToSend);
}

export const handleJoin = (roomName: string, userName: string, socket: Socket, io: ioServer) => {
  const room: Room | undefined = rooms.find(room => room.roomName === roomName);
  if (room?.roomName) {
    socket.join(roomName);

    // if user already has a room
    if (userRoomMapping.has(userName)) {
      const userRoom: Room | undefined = userRoomMapping.get(userName);
      userRoom!.users = userRoom!.users!.filter(roomUser => roomUser.userName !== userName);
    }
    room?.users?.push({ userName, progress: new Progress });
    if (room?.users?.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
      room.maxUsersReached = true;
    }
    userRoomMapping.set(userName, room);
    socket.emit(actions.userJoined, room);

    
    socket.broadcast.to(room!.roomName).emit(actions.roomUpdate, room);
    updateRooms(io);
  }
}

export const handleCreateRoom = (roomName: string, username: string, socket: Socket, io: ioServer): void => {
  if (!roomName || !username) {
    return;
  }
  
  for (let room of rooms) {
    if (room.roomName === roomName) {
      socket.emit(actions.roomNameIsNotUnique, 'Room name is not unique');
      return;
    }
  }

  rooms.push({ roomName, users: [], maxUsersReached: false, state: 'pending' });
  handleJoin(roomName, username, socket, io);
}


export const getGameResult = (room: Room): GameResult[] => {
  const result: GameResult[] = room.users!.map((user: User) => {
    return ({ 
      username: user.userName, 
      spentSeconds: user?.progress?.spentSeconds 
    });
  });
  return result.sort((result1, result2) => (result1.spentSeconds || Infinity) - (result2.spentSeconds || Infinity));
}

export const checkAllReady = (room: Room, io: ioServer) => {
  if (room?.users?.every(user => user.progress.isReady) && room.state === 'pending') {
    room.state = 'timer_started';
    const textIdx = getRandomNumber(texts.length);

    io.to(room.roomName).emit(actions.usersReady, {
        seconds: SECONDS_TIMER_BEFORE_START_GAME,
        textIdx
    });
    room.text = texts[textIdx];

    updateRooms(io);
    setTimeout(() => {
      io.to(room.roomName).emit(actions.chalengeStart, SECONDS_FOR_GAME);
      room.state = 'game_started';

      const timeOutId: NodeJS.Timeout = setTimeout(() => {
        if (room.state === 'game_started') {
          io.to(room.roomName).emit(actions.chalengeEnd, getGameResult(room));
          room.state = 'pending';
        }
      }, SECONDS_FOR_GAME * 1000);
      roomIntervalMapping.set(room.roomName, timeOutId);
    }, SECONDS_TIMER_BEFORE_START_GAME * 1000)
  }
}

export const checkAllUsersDone = (room: Room, io: ioServer): void => {
  if (room.state === 'game_started' && room.users?.every(user => user.progress.doneInPerc === 100)) {
    const timeoutId: NodeJS.Timeout | undefined = roomIntervalMapping.get(room.roomName);
    if (timeoutId) {
      clearTimeout(timeoutId);
    }
    const result: GameResult[] = getGameResult(room); 
    io.to(room.roomName).emit(actions.chalengeEnd, result);

    room.text = undefined;
    room.users.forEach(user => user.progress = new Progress);
    room.state = 'pending';
    io.to(room.roomName).emit(actions.roomUpdate, room);
    updateRooms(io);
  }
}

export const handleLeaveRoom = (userName: string, socket: Socket, io: ioServer): void => {
  const userRoom: Room | undefined = userRoomMapping.get(userName);
  if (userRoom) {
    socket.leave(userRoom!.roomName);
    userRoomMapping.delete(userName);
  }

  if (userRoom?.users) {
    userRoom!.users = userRoom?.users?.filter(roomUser => roomUser.userName !== userName);
    // if room has no users
    if (!userRoom?.users?.length) {
      const emptyRoomIdx = rooms.findIndex(room => room.roomName === userRoom.roomName);
      rooms.splice(emptyRoomIdx, 1);
    } else {
      if (userRoom?.users?.length !== MAXIMUM_USERS_FOR_ONE_ROOM) {
        userRoom.maxUsersReached = false;
      }
      io.to(userRoom.roomName).emit(actions.roomUpdate, userRoom);
      checkAllReady(userRoom, io);
      checkAllUsersDone(userRoom, io);
    }
    updateRooms(io);
  };
};

export const handleUserStateUpdate = (userName: string, progress: Progress, io: ioServer): void => {
  const userRoom: Room | undefined = userRoomMapping.get(userName);
  if (userRoom?.roomName) {
    const currUser: User | undefined = userRoom.users?.find(user => user.userName === userName);
    progress.doneInPerc = calcProgress(userRoom.text!, progress.inputtedText);
    Object.assign(currUser!.progress, progress);
    io.to(userRoom.roomName).emit(actions.userStateUpdate, { userName, progress });
    checkAllUsersDone(userRoom, io);
  }
}

export const handleToggleReady = (userName: string, progress: Progress, io: ioServer): void => {
  const userRoom: Room | undefined = userRoomMapping.get(userName);
  handleUserStateUpdate(userName, progress, io);
  checkAllReady(userRoom!, io)
};

export const handleUserDisconnect = (userName: string, socket: Socket, io: ioServer): void => {
  connectedUsers.delete(userName);
  handleLeaveRoom(userName, socket, io);
}
