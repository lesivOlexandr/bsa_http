import { Server as ioServer, Socket } from 'socket.io';
import { actions } from '../constants/actions';
import { Progress } from '../types'
import { connectedUsers} from '../shared';
import { updateRooms, handleJoin, handleCreateRoom, handleUserDisconnect, handleLeaveRoom, handleToggleReady, handleUserStateUpdate } from './handlers/gameHandlers';

export default (io: ioServer) => {
  io.on("connection", (socket: Socket) => {
    const username = socket.handshake.query.username;

    if (connectedUsers.has(username)) {
      socket.emit(actions.userNameIsNotUnique, 'User name is not unique');
    } else {
      connectedUsers.add(username);
      updateRooms(io);
    }

    socket.on('disconnect', () => handleUserDisconnect(username, socket, io));
    socket.on(actions.createRoom, (roomName: string) => handleCreateRoom(roomName, username, socket, io));
    socket.on(actions.joinRoom, (roomName: string) => handleJoin(roomName, username, socket, io));
    socket.on(actions.leaveRoom, () => handleLeaveRoom(username, socket, io));
    socket.on(actions.toggleReady, (progress: Progress) => handleToggleReady(username, progress, io));
    socket.on(actions.userStateUpdate, (progress: Progress) => handleUserStateUpdate(username, progress, io));
  });
};
