import express, {Express} from 'express';
import http, {Server} from 'http';
import socketIO, {Server as ioServer} from 'socket.io';

import {PORT, STATIC_PATH, JS_FILES_PATH} from './config';
import routes from './routes';
import socketHandler from './socket';

const app: Express = express();
const httpServer: Server = new http.Server(app);
const io: ioServer = socketIO(httpServer);

app.use(express.static(STATIC_PATH));
routes(app);

app.get('*', (_, res) => res.redirect('/login'));

socketHandler(io);

httpServer.listen(PORT, () => {
  console.log(`Listen server on port ${PORT}`);
})
 