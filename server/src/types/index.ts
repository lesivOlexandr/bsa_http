export interface Room {
  roomName: string;
  text?: string;
  usersCount?: number;
  users?: User[];

  maxUsersReached?: boolean;
  state?: 'pending' | 'timer_started' | 'game_started';
}

export class Progress {
  constructor(
    public isReady: boolean = false,
    public inputtedText: string = '',
    public doneInPerc: number = 0,
    public spentSeconds: number = 0
  ){}
}

export interface User {
  userName: string;
  progress: Progress;
}

export interface GameResult {
  username: string,
  spentSeconds: number | null
}