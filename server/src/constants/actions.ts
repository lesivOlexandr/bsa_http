export enum actions {
  // incoming
  createRoom = 'CREATE_ROOM',
  joinRoom = 'JOIN_ROOM',
  leaveRoom = 'LEAVE_ROOM',
  toggleReady = 'TOGGLE_READY',

  //outcoming
  updateRooms = 'UPDATE_ROOMS',
  userNameIsNotUnique = 'ERROR_USER_NAME_IS_NOT_UNIQUE',
  roomNameIsNotUnique = 'ERROR_ROOM_NAME_IS_NOT_UNIQUE',
  error = 'ERROR',
  userJoined = 'USER_JOINED',
  roomUpdate = 'ACTIVE_ROOM_UPDATE',
  userStateUpdate = 'USER_STATE_UPDATE',
  usersReady = 'ALL_ROOM_USERS_READY',
  chalengeStart = 'START_CHALLENGE',
  chalengeEnd = 'END_CHALLENGE'
}