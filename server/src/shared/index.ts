import { Room } from "../types";

export const connectedUsers: Set<string> = new Set;
export let rooms: Room[] = [];
export const userRoomMapping: Map<string, Room> = new Map;
export const roomIntervalMapping: Map<string, NodeJS.Timeout> = new Map;